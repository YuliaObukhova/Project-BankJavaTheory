import java.util.Scanner;

public class ProjectBank {
    public static void main(String[] args) {
        final double ROUBLES_PER_CAD = 58.76; // курс покупки

        int cad; // сумма денег в канадских долларах
        double roubles; // сумма денег в российских рублях
        int digit; // последняя цифра cad
        int kolvo; // количество конвертаций введённое пользователем
        int i; // счётчик
        Scanner input = new Scanner(System.in);

        do {
            System.out.println("Введите корректное количество конвертаций: ");
            kolvo = input.nextInt();
        } while
        (kolvo <= 0);
        for (i = 0; i < kolvo; ++i) {
            System.out.println("Введите сумму денег в канадских долларах: ");
            cad = input.nextInt();
            System.out.print(cad);

            if (5 <= cad && cad <= 20)
                System.out.print(" канадских долларов равны ");

            else {
                digit = cad % 10;

                if (digit == 1) {
                    System.out.print(" канадский доллар равен ");
                } else if (2 <= digit && digit <= 4) {
                    System.out.print(" канадских доллара равны ");
                } else {
                    System.out.print(" канадских долларов равны ");
                }
            }
            roubles = ROUBLES_PER_CAD * cad;
            System.out.println((int)(roubles * 100) / 100.0
                    + " российского рубля.");
        }
    }
}
